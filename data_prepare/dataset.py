import cv2
import torch
import numpy as np
from tqdm import tqdm
from torchvision import transforms

from data_prepare.video import ProcessVideo
from data_prepare.feature_extract import ProcessSimpleNN
from PIL import Image


class InstantDataset:
    data_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])
    ])

    def __init__(self, video_path):
        self.video_path = video_path
        self.preprocess = ProcessSimpleNN()

        self.capture = cv2.VideoCapture(self.video_path)

    def load(self, batch_size):
        with ProcessVideo(capture=self.capture) as frames:
            frames_result = []
            for frame_ind, frame in tqdm(frames):
                frame_processed = self.preprocess.build(frame)
                frame_processed = Image.fromarray(np.uint8(frame_processed)).convert('RGB')
                frame_processed = self.data_transform(frame_processed).numpy()
                frames_result.append(frame_processed)

                if frame_ind != 0 and (frame_ind + 1) % batch_size == 0:
                    previous_frames = frames_result
                    frames_result = []
                    yield torch.tensor(np.asarray(previous_frames))
            yield torch.tensor(np.asarray(frames_result))


class InstantLoader:
    def __init__(self, dataset, n_batch):
        self.dataset = dataset
        self.n_batch = n_batch

        self.load_dataset = self.dataset.load(n_batch)

    def __iter__(self):
        return self

    def __next__(self):
        while self.dataset:
            result = next(self.load_dataset)
            return result, None

        raise StopIteration()
