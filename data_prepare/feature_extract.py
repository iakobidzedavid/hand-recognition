import cv2


class ProcessSimpleNN:

    def _resize(self, img):
        scale_percent = 20  # percent of original size
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
        return resized

    def build(self, frame):
        resized_frame = self._resize(frame)
        return resized_frame