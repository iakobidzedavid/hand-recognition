
class ProcessVideo:
    def __init__(self, capture):
        self.capture = capture

    def release(self):
        ret, frame = self.capture.read()
        return ret, frame

    def __enter__(self):
        frame_index = 0
        while self.capture.isOpened():
            ret, frame = self.release()
            if ret is False:
                break
            yield frame_index, frame
            frame_index += 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.capture.release()
