# soapy

### Getting Started

Firstly, you need to have at least:

* Python3.5
* cv2 
* 8 GB RAM

You can run project either in CPU (slower) or GPU mode. To run it on GPU make sure that you have CUDA drivers installed.

* CUDA 9.3 (10.1)

You need to install required libraries on your server. It's better to do with virtual environment. Type: 

```
pip3 install virtualenv
virtualenv -p python3 venv
source venv/bin/activate
pip3 install -r requirements.txt
```

## How to test with Flask
```
Make HTTP-POST request to <URL>/hand-recognition with file: video
```

## How to Run (Without Flask)

### Hands poses detection

To detect pose you need to download this [model](https://drive.google.com/file/d/1BjAgS-HyKo1nkmUEodxUiqQ-4trKZBdj/view?usp=sharing).
Go to drive and save file on your computer. You have to provide path to the checkpoints when you will run detection. 
To run execution of hands poses from script, type:

```
python3 detect.py <video_path> <model_path> 
```

E.g. 

```
python3 detect.py ../data/08-09-20_06-40-11.h264 ../data/model/checkpoints
```

To run from program
```
from models.detection import HandsDetection
from models.resnet18 import ResNetDetect

# this will upload model weights
model = ResNetDetect(model_path=<path_to_checkpoints>)

# this commands allows evaluate detection and summarize result for one video
detector = HandsDetection(model)
result = detector.detect(<video_path>)

```

If you have more RAM memory or gpu, you can increase the n_batch param.

```
model = ResNetDetect(<path_to_checkpoints>, n_batch=64)
```

From `result` variable you can get dictionary as in example below: 
```
{
    "Between fingers": True,
    "Back of fingers": True,
    "Fingernail left": False,
    ...
}
``` 