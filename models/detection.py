import numpy as np

from settings import LEARN_DESC_HANDS


class HandsDetection:

    def __init__(self, model=None):
        self.model = model

        self.results = []
        self.summary = None

    def detect(self, video_path, step=3):
        y_pred = self.model.run(video_path)
        y_pred = self._vote_window(y_pred, step=step)

        summary = np.unique(y_pred)
        return {v: True if k in summary else False for k, v in LEARN_DESC_HANDS.items()}

    def _vote_window(self, y, step):
        result_pred = np.zeros(y.shape, dtype=np.int)
        for ind in range(len(y)):
            half_step = int(step / 2)

            ind_min = ind - half_step
            ind_min = ind_min if ind_min > 0 else 0

            ind_max = ind + half_step
            ind_max = ind_max if ind_max < len(y) else len(y)

            window = y[ind_min: ind_max]
            max_v = np.bincount(window).argmax()
            result_pred[ind] = int(max_v)
        return result_pred

    def _interpolation_vote(self, step):
        results_n = len(self.results)
        result_pred = np.zeros(results_n, dtype=np.int)
        for ind in range(results_n):
            half_step = int(step / 2)

            ind_min = ind - half_step
            ind_min = ind_min if ind_min > 0 else 0

            ind_max = ind + half_step
            ind_max = ind_max if ind_max < results_n else results_n

            window = self.results[ind_min: ind_max]
            max_v = np.bincount(window).argmax()
            result_pred[ind] = int(max_v)
        return result_pred

    def _evaluate_postprocess(self, step=3):
        y_pred = self._interpolation_vote(step)
        y_pred_unique = np.unique(y_pred)
        return y_pred_unique
