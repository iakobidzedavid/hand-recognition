import torch
import torch.nn as nn

from torchvision import models
from models.utils import test_model, device
from data_prepare.dataset import InstantDataset, InstantLoader


class ResNetDetect:

    def __init__(self, model_path, n_batch=32):
        self.model_nn = model_path

        self.datasets_cls = InstantDataset
        self.loaders_cls = InstantLoader
        self.n_batch = n_batch

    @property
    def model_nn(self):
        return self._model_nn

    @model_nn.setter
    def model_nn(self, model_path):
        self._model_nn = models.resnet18(pretrained=True)
        self._model_nn.fc = nn.Linear(self._model_nn.fc.in_features, 12)
        self._model_nn.to(device)
        self._model_nn.load_state_dict(torch.load(model_path, map_location=torch.device(device)))

    def run(self, video_name):
        loader = self.loaders_cls(self.datasets_cls(video_name), self.n_batch)
        preds, _ = test_model(self.model_nn, loader)
        return preds
