import numpy as np
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def test_model(model, loader):
    result_preds = []
    result_target = []

    for data, target in loader:
        data = data.to(device)
        target.to(device) if target else None
        model.eval()
        output = model(data)
        _, preds = torch.max(output, 1)
        result_preds.extend([t.item() for t in preds])

        if result_target:
            result_target.append([t.item() for t in target])
    return np.asarray(result_preds), np.asarray(result_target)
