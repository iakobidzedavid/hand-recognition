import hashlib
import json
import os
from pathlib import Path

from flask import Flask, request, Response
from flask_restful import Api, Resource

from models.detection import HandsDetection
from models.resnet18 import ResNetDetect

app = Flask(__name__)
api = Api(app)

MODEL_PATH = 'checkpoints/checkpoints'
N_BATCHES = 32

VIDEOS_PATH = Path("./videos")
VIDEOS_PATH.mkdir(parents=True, exist_ok=True)


def detect(video_path):
    # Detect hand poses
    hands_detection = HandsDetection(ResNetDetect(model_path=MODEL_PATH, n_batch=N_BATCHES))
    result = hands_detection.detect(video_path)

    # Remove used video
    os.remove(video_path)

    print('For video {} result is next: \n {}'.format(os.path.basename(video_path), json.dumps(result, indent=4)))
    result = json.dumps(result)
    return Response(result, 200, mimetype='application/json')


class HandPoseDetection(Resource):
    def post(self):
        video = request.files['file']
        video_path = '{0}/{1}_{2}'.format(VIDEOS_PATH, hashlib.md5().hexdigest(), video.filename)
        # Save video to local disk
        with open(video_path, 'wb') as file:
            file.write(video.read())

        return detect(video_path)


api.add_resource(HandPoseDetection, '/hand-recognition')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
