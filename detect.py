import click
import json
import os

from models.detection import HandsDetection
from models.resnet18 import ResNetDetect


@click.command()
@click.argument('video_path')
@click.argument('model_path')
@click.option('-bn', '--n_batches', type=int, default=32,
              help='Number of frame for step of evaluating. Can be increased'
                   'in GPU mode')
def detect(video_path, model_path, n_batches):
    hands_detection = HandsDetection(ResNetDetect(model_path=model_path, n_batch=n_batches))
    result = hands_detection.detect(video_path)

    print('For video {} result is next: \n {}'.format(os.path.basename(video_path), json.dumps(result, indent=4)))


if __name__ == '__main__':
    detect()
