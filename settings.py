
LEARN_DESC_HANDS = {
    0: "Palm to palm",
    1: "Between fingers",
    2: "Back of left hand",
    3: "Back of right hand",
    4: "Base of left thumb",
    5: "Base of right thumb",
    6: "Back of fingers",
    7: "Fingernail left",
    8: "Fingernail right",
    9: "Wrist left",
    10: "Wrist right",
    11: "Other"
}
